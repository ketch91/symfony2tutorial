<?php

use Doctrine\Common\Annotations\AnnotationRegistry;

$loader = require __DIR__.'/../vendor/autoload.php';
$loader->add('Admingenerator', array(__DIR__.'/../src', __DIR__.'/../vendor/bundles') );
$loader->add('Knp', array(__DIR__.'/../vendor/bundles'));
$loader->add('Knp\\Menu', array(__DIR__.'/../vendor/KnpMenu/src'));

// intl
if (!function_exists('intl_get_error_code')) {
    require_once __DIR__.'/../vendor/symfony/symfony/src/Symfony/Component/Locale/Resources/stubs/functions.php';
}

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
