<?php

namespace Blogger\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Admingenerator\GeneratorBundle\Menu\AdmingeneratorMenuBuilder;

class AdminController extends Controller
{
    public function indexAction()
    {
        return $this->render('BloggerAdminBundle:Admin:index.html.twig', array('name' => 'raz1'));
    }
}
